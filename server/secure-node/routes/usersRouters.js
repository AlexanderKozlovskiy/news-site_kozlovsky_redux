const express = require("express");
const router = express.Router();
const usersController = require("../controllers/usersController");
router.post("/register", usersController.registration);
router.post("/signIn", usersController.login);
router.get("/list", usersController.usersList);
module.exports = router;
