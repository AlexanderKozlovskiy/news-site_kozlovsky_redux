const request = require("supertest");
const app = require("../app");
const newsList = require("../controllers/newsController").list;

const mockResponse = () => {
  const res = {};
  res.send = jest.fn().mockReturnValue(res);
  res.status = jest.fn().mockReturnValue(res);
  res.json = jest.fn().mockReturnValue(res);
  return res;
};

const mockRequest = body => ({
  body,
});

const newsResponseBody = {
  title: "title",
  description: "description",
  user_id: 1,
  tags: "title",
  createdAt: "2005-04-02T19:00:00.000Z",
  updatedAt: "2005-04-02T19:00:00.000Z",
  image: "image",
};

jest.mock("../query/news", () => {
  return {
    getNews: () => {
      return new Promise(res => {
        res(newsResponseBody);
      });
    },
  };
});

describe("GET /news", () => {
  test("It should response the GET method", done => {
    request(app)
      .get("/news")
      .then(response => {
        expect(response.statusCode).toBe(200);
        done();
      });
  });
  test("test defined response body", async () => {
    const { body } = await request(app).get("/news");

    expect(body).toEqual({
      title: expect.any(String),
      user_id: expect.any(Number),
      description: expect.any(String),
      tags: expect.any(String),
      createdAt: expect.any(String),
      updatedAt: expect.any(String),
      image: expect.any(String),
    });
  });
});

describe("newsList", () => {
  test("should return  status 200", async () => {
    const res = mockResponse();
    const req = mockRequest({});
    await newsList(req, res, () => {});
    expect(res.status).toHaveBeenCalledWith(200);
    expect(res.send).toHaveBeenCalledWith(newsResponseBody);
  });
});
