const signIn = require("../controllers/usersController").login;
const jwt = require("jsonwebtoken");

const mockResponse = () => {
  const res = {};
  res.send = jest.fn().mockReturnValue(res);
  res.status = jest.fn().mockReturnValue(res);
  res.json = jest.fn().mockReturnValue(res);
  return res;
};

const mockRequest = body => ({
  body,
});

const usersList = [
  {
    id: 1,
    first_name: "Astix",
    last_name: "Fivennd",
    e_mail: "asd@asd.ru",
    password: "$2b$10$yVyPiKANjmHG3YvclTgmretmJYlTJZSoh1tKElDtAa/O0d9TF6dXO",
    photo: "1111",
    createdAt: "2005-04-02T19:00:00.000Z",
    updatedAt: "2005-04-02T19:00:00.000Z",
  },
];

const newsList = [
  {
    title: "title",
    description: "description",
    user_id: 1,
    tags: "title",
    createdAt: "2005-04-02T19:00:00.000Z",
    updatedAt: "2005-04-02T19:00:00.000Z",
    image: "image",
  },
];

const loginValidObject = {
  email: "asd@asd.ru",
  password: "123123",
};

const loginInvalidRequestObject = {
  email: "somemail@asd.ru",
  password: "impossiblepassword",
};

jest.mock("../models/users.js", () => () => {
  const SequelizeMock = require("sequelize-mock");
  const dbMock = new SequelizeMock();
  return dbMock.define("user", usersList[0]);
});

jest.mock("../models/news.js", () => () => {
  const SequelizeMock = require("sequelize-mock");
  const dbMock = new SequelizeMock();
  return dbMock.define("news", newsList);
});

jest.mock("../query/users.js", () => {
  return {
    getUserByEmail: email => {
      if (email === loginInvalidRequestObject.email) {
        return new Promise(res => res(null));
      }
      return new Promise(res => res({ dataValues: usersList[0] }));
    },
    getUsers: () => {
      return Promise.resolve(usersList);
    },
  };
});

describe("users controller", () => {
  test("res.json return 'User not found'", async () => {
    const res = mockResponse();
    const req = mockRequest(loginInvalidRequestObject);
    await signIn(req, res, () => {});
    expect(res.status).toHaveBeenCalledWith(400);
    expect(res.json).toHaveBeenCalledWith("User not found");
  });

  test("res.status should return 200", async () => {
    const res = mockResponse();
    const req = mockRequest(loginValidObject);
    await signIn(req, res, () => {});
    expect(res.status).toHaveBeenCalledWith(200);
  });
});
