const { Users } = require("../models/");
const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt");
const { getUsersByEmail, getUserByEmail, getUsers } = require("../query/users");
const { promisify } = require("util");

module.exports = {
  async registration(req, res) {
    try {
      const user = await getUsersByEmail(req.body.email);
      if (user.length !== 0) {
        return res.status(400).send("Email already exists");
      } else {
        const newUser = new Users({
          e_mail: req.body.email,
          password: req.body.password,
          first_name: req.body.firstName,
          last_name: req.body.lastName,
          photo: req.body.photo,
        });
        bcrypt.genSalt(10, (err, salt) => {
          if (err) console.error("genSalt error", err);
          else {
            bcrypt.hash(newUser.password, salt, (err, hash) => {
              if (err) console.error("bcrypt error", err);
              else {
                newUser.password = hash;
                newUser
                  .save()
                  .then(user => {
                    const payload = {
                      id: user.id,
                      e_mail: user.e_mail,
                      last_name: user.last_name,
                      first_name: user.first_name,
                      photo: user.photo,
                    };
                    jwt.sign(
                      payload,
                      "secret",
                      {
                        expiresIn: 7200,
                      },
                      (err, token) => {
                        if (err) console.error("There is some error in token", err);
                        else {
                          res.status(200).json({
                            success: true,
                            token: `Bearer ${token}`,
                          });
                        }
                      }
                    );
                  })
                  .catch(error => console.error(error));
              }
            });
          }
        });
      }
    } catch (err) {
      res.send("bad registration").status(500);
    }
  },

  async login(req, res) {
    const { email, password } = req.body;
    try {
      const user = await getUserByEmail(email);
      if (!user) {
        return res.status(400).json("User not found");
      }
      let userValue = user.dataValues;
      const isMatch = await bcrypt.compare(password, userValue.password);
      if (isMatch) {
        const payload = {
          id: userValue.id,
          email: email,
          first_name: userValue.first_name,
          last_name: userValue.last_name,
        };
        try {
          const token = await promisify(jwt.sign)(payload, "secret", {
            expiresIn: 7200,
          });
          return res.status(200).json({
            success: true,
            token: `Bearer ${token}`,
          });
        } catch (error) {
          return res.status(403).json("wrong password");
        }
      } else {
        res.status(403).json("wrong password");
      }
    } catch (e) {
      res.status(403).json("bad authorization");
    }
  },

  async usersList(req, res) {
    try {
      const users = await getUsers();
      return res.status(200).send(users);
    } catch (error) {
      return res.status(400).send(error);
    }
  },
};
