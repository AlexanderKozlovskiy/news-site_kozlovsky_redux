const { Users } = require("../models/");
const { getNews } = require("../query/news");
const { getUsers } = require("../query/users");

module.exports = {
  async list(req, res) {
    try {
      const news = await getNews();
      return res.status(200).send(news);
    } catch (error) {
      return res.status(500).send("Error getting news from the database. Try again ");
    }
  },
};
