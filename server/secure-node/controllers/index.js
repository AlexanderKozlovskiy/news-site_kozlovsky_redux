const news = require("./news");
const users = require("./users");

module.exports = {
  news,
  users,
};
