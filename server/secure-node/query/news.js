const { News, Users } = require("../models");

module.exports = {
  getNews() {
    return News.findAll({
      include: [
        {
          model: Users,
          as: "author",
          attributes: ["first_name", "last_name", "e_mail"],
        },
      ],
    });
  },
};
