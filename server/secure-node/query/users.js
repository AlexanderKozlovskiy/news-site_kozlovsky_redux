const { Users } = require("../models");

module.exports = {
  getUsers() {
    return Users.findAll();
  },

  getUsersByEmail(email) {
    return Users.findAll({
      where: {
        e_mail: email,
      },
    });
  },

  getUserByEmail(email) {
    return Users.findOne({
      where: {
        e_mail: email,
      },
    });
  },
};
