import { runSaga } from "redux-saga";

export const recordSaga = async function (sagaHandler, initialAction) {
  const dispatchedActions = [];
  const fakeStore = {
    getState: () => initialState,
    dispatch: action => dispatchedActions.push(action),
  };
  await runSaga(fakeStore, sagaHandler, initialAction).done;
  return dispatchedActions;
};
