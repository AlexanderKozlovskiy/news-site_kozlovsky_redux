/* eslint-disable */
import axios from "axios";

export const fetchNews = async () => {
  const data = await axios("http://localhost:4000/news");
  return data;
};
