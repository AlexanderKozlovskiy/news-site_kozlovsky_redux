import axios from "axios";

export const registerUser = async user => {
  const data = await axios("http://localhost:4000/users/register", {
    method: "post",
    data: user,
  });
  return data;
};

export const loginUser = async action => {
  const data = await axios("http://localhost:4000/users/signIn", {
    method: "post",
    data: action,
  });
  return data;
};
