const { render } = require("@testing-library/react");
const { useSelector } = require("react-redux");
const { default: ListNews } = require("../../views/components/ListNews");

jest.mock("react-redux");
let news;
beforeEach(() => {
  news = [
    { id: 1, title: "" },
    { id: 2, title: "" },
    { id: 3, title: "" },
    { id: 4, title: "" },
  ];
});

describe("ListNews", () => {
  test("should render all received news ", () => {
    useSelector.mockImplementation(cb => cb({ news: { newsError: "" } }));
    const { getAllByTestId } = render(<ListNews news={news} />);

    const cards = getAllByTestId("news-card");
    expect(cards).toHaveLength(news.length);
  });
});
