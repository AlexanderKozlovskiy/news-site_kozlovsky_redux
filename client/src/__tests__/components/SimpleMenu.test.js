const { Menu } = require("@material-ui/core");
const { render, fireEvent } = require("@testing-library/react");
const { useSelector, useDispatch } = require("react-redux");
const { default: SimpleMenu } = require("../../views/components/SimpleMenu");

jest.mock("react-redux");

describe("SimpleMenu  component", () => {
  test("should render sign-out button", () => {
    useSelector.mockImplementation(cb => cb({ users: { isAuth: true } }));
    const { getByTestId } = render(<SimpleMenu />);
    const menu = getByTestId("simple-menu");
    const signOutButton = getByTestId("sign-out");
    expect(menu).toContainElement(signOutButton);
  });

  test("should render sign-in list item", () => {
    useSelector.mockImplementation(cb => cb({ users: { isAuth: false } }));
    const { getByTestId } = render(<SimpleMenu />);
    const menu = getByTestId("simple-menu");

    const signInButton = getByTestId("login");
    expect(menu).toContainElement(signInButton);
  });
});
