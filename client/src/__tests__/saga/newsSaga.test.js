import * as api from "../../services/news";
import { getNewsError, getNewsSuccess } from "../../stores/action/newsAction";
import { HANDLER } from "../../stores/sagas/newsSaga";
import { recordSaga } from "../../services/utils";

describe("newsSaga", () => {
  test("should call api and dispatch success action", async () => {
    const dummyNews = {
      data: {
        createdAt: Date.now().toString(),
        description: "some description",
        id: 1,
        tags: "some tags",
        title: "some title",
        updatedAt: Date.now().toString(),
        user_id: 1,
        user: {
          e_mail: "a@a.ru",
          first_name: "Alexander",
          last_name: "Kozlovskiy",
        },
      },
    };
    const requestNews = jest
      .spyOn(api, "fetchNews")
      .mockImplementation(() => Promise.resolve(dummyNews));

    const dispatched = await recordSaga(HANDLER.GET_NEWS);

    expect(requestNews).toHaveBeenCalledTimes(1);
    expect(dispatched).toContainEqual(getNewsSuccess(dummyNews.data));
    requestNews.mockClear();
  });

  test("should call api and dispatch error action", async () => {
    const requestNews = jest
      .spyOn(api, "fetchNews")
      .mockImplementation(() => Promise.reject("failed"));

    const dispatched = await recordSaga(HANDLER.GET_NEWS);

    expect(requestNews).toHaveBeenCalledTimes(1),
      expect(dispatched).toContainEqual(getNewsError("failed"));
    requestNews.mockClear();
  });
});
