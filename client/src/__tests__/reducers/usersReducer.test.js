import { initialState, usersReducer } from "../../stores/reducers/usersReducer";

test("should return the initial state", () => {
  expect(usersReducer(undefined, {})).toEqual(initialState);
  expect(usersReducer(undefined, null)).toEqual(initialState);
  expect(usersReducer(undefined, undefined)).toEqual(initialState);
});
