import { createStore } from "redux";
import { rootReducer } from "../../stores/reducers/rootReducer";
import { initialState as initialUsersState } from "../../stores/reducers/usersReducer";
import { initialState as initialNewsState } from "../../stores/reducers/newsReducer";

let store = createStore(rootReducer);

test("should return initial state", () => {
  expect(store.getState().users).toEqual(initialUsersState);
  expect(store.getState().news).toEqual(initialNewsState);
});
