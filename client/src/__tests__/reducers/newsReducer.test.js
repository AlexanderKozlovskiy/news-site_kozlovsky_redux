import {
  GET_NEWS_SUCCESS,
  GET_NEWS_ERROR,
  HANDLE_SEARCH_VALUE,
} from "../../stores/constants/constants";
import { initialState, newsReducer } from "../../stores/reducers/newsReducer";

const payload = [
  { id: 0, title: "some Title", description: "some description" },
  { id: 1, title: "some Title", description: "some description" },
];

test("should return the initial state", () => {
  expect(newsReducer(undefined, {})).toEqual(initialState);
  expect(newsReducer(undefined, null)).toEqual(initialState);
  expect(newsReducer(undefined, undefined)).toEqual(initialState);
});

test("should return new object with payload", () => {
  expect(newsReducer(initialState, { type: GET_NEWS_SUCCESS, payload })).toHaveProperty(
    "allNews",
    payload
  );
  expect(
    newsReducer(initialState, {
      type: GET_NEWS_ERROR,
      payload: "some sting error",
    })
  ).toHaveProperty("newsError", "some sting error");
  expect(
    newsReducer(initialState, { type: HANDLE_SEARCH_VALUE, payload: "search" })
  ).toHaveProperty("searchedValue", "search");
});
