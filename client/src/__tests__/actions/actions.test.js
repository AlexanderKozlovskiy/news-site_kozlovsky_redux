describe("action creators", () => {
  test("should return action object", () => {
    const MOCK_ACTION_TYPE = "MOCK_ACTION";
    const MOCK_ACTION_PAYLOAD = "some string";
    const mockActionCreator = jest.fn(payload => ({
      type: MOCK_ACTION_TYPE,
      payload,
    }));

    expect(mockActionCreator().payload).toBeFalsy();
    expect(mockActionCreator(MOCK_ACTION_PAYLOAD).payload).not.toBeNull();
    expect(mockActionCreator(MOCK_ACTION_PAYLOAD)).toEqual({
      type: MOCK_ACTION_TYPE,
      payload: MOCK_ACTION_PAYLOAD,
    });
  });
});
