/* eslint-disable */
import { combineReducers } from "redux";
import { newsReducer } from "./newsReducer";
import { usersReducer } from "./usersReducer";

export const rootReducer = combineReducers({
  news: newsReducer,
  users: usersReducer,
});
