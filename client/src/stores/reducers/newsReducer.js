import { GET_NEWS_ERROR, GET_NEWS_SUCCESS, HANDLE_SEARCH_VALUE } from "../constants/constants";

export const initialState = {
  allNews: [],
  newsError: "",
  currentNews: [],
  searchedValue: "",
};

export const newsReducer = (state = initialState, action) => {
  switch (action?.type) {
    case GET_NEWS_SUCCESS:
      return {
        ...state,
        allNews: action.payload,
      };
    case GET_NEWS_ERROR:
      return {
        ...state,
        newsError: action.payload,
      };
    case HANDLE_SEARCH_VALUE:
      return {
        ...state,
        searchedValue: action.payload,
      };
    default:
      return { ...state };
  }
};
