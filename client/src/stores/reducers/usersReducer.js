import { SET_CURRENT_USER } from "../constants/constants";

export const initialState = {
  usersError: "",
  currentUser: {},
  isAuth: false,
};

export const usersReducer = (state = initialState, action) => {
  switch (action?.type) {
    case SET_CURRENT_USER: {
      const currentAuth = !!(action.payload !== null && action.payload.id);
      return {
        ...state,
        currentUser: action.payload,
        isAuth: currentAuth,
      };
    }
    default:
      return { ...state };
  }
};
