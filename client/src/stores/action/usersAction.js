import {
  REGISTER_USER,
  AUTHORIZATION_USER,
  GET_USERS_ERROR,
  SET_CURRENT_USER,
  GET_USERS,
  LOGOUT_USER,
} from "../constants/constants";

export const registerUser = payload => ({
  type: REGISTER_USER,
  payload,
});

export const authorizationUser = payload => ({
  type: AUTHORIZATION_USER,
  payload,
});
export const getUsersError = payload => ({
  type: GET_USERS_ERROR,
  payload,
});
export const setCurrentUser = payload => ({
  type: SET_CURRENT_USER,
  payload,
});
export const getUsers = payload => ({
  type: GET_USERS,
  payload,
});
export const logout = () => ({
  type: LOGOUT_USER,
});
