import {
  GET_NEWS,
  GET_NEWS_SUCCESS,
  GET_NEWS_ERROR,
  FILTER_NEWS,
  HANDLE_SEARCH_VALUE,
} from "../constants/constants";

export const getNews = () => ({
  type: GET_NEWS,
});

export const getNewsSuccess = payload => ({
  type: GET_NEWS_SUCCESS,
  payload,
});

export const getNewsError = payload => ({
  type: GET_NEWS_ERROR,
  payload,
});

export const filterNews = payload => ({
  type: FILTER_NEWS,
  payload,
});

export const handleSearchValue = payload => ({
  type: HANDLE_SEARCH_VALUE,
  payload,
});
