import { all, fork } from "redux-saga/effects";

import { sagaWatcher } from "./newsSaga";
import sagaWatcherUsers from "./userSaga";

function* rootSaga() {
  yield all([fork(sagaWatcher), fork(sagaWatcherUsers)]);
}

export default rootSaga;
