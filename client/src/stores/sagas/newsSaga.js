import { call, put, takeEvery } from "redux-saga/effects";
import { fetchNews } from "../../services/news";
import { getNewsError, getNewsSuccess } from "../action/newsAction";
import { GET_NEWS } from "../constants/constants";

export const HANDLER = {
  *[GET_NEWS]() {
    try {
      const response = yield call(fetchNews);
      yield put(getNewsSuccess(response.data));
    } catch (error) {
      yield put(getNewsError(error));
    }
  },
};

export function* sagaManage({ type, payload }) {
  const handler = HANDLER[type];
  if (handler) {
    yield handler(payload);
  }
}
export function* sagaWatcher() {
  yield takeEvery("*", sagaManage);
}
