import { call, put, takeEvery } from "redux-saga/effects";
import jwt_decode from "jwt-decode";
import { setCurrentUser, getUsersError, registerUser } from "../action/usersAction";
import { REGISTER_USER, AUTHORIZATION_USER } from "../constants/constants";
import setAuthToken from "./setAuthToken";
import { loginUser } from "../../services/users";

const HANDLER_USER = {
  *[REGISTER_USER](payload) {
    try {
      const user = payload;
      const res = yield call(registerUser, user);
      const { token } = res.data;
      localStorage.setItem("jwtToken", token);
      setAuthToken(token);
      const decoded = jwt_decode(token);
      localStorage.setItem("user", JSON.stringify(decoded));
      yield put(setCurrentUser(decoded));
    } catch (err) {
      yield put(getUsersError(err));
    }
  },
  *[AUTHORIZATION_USER](action) {
    try {
      const res = yield call(loginUser, action);
      const { token } = res.data;
      localStorage.setItem("jwtToken", token);
      setAuthToken(token);
      const decoded = jwt_decode(token);
      localStorage.setItem("user", JSON.stringify(decoded));
      yield put(setCurrentUser(decoded));
    } catch (err) {
      yield put(getUsersError(err.response.data));
    }
  },
};
function* sagaManage({ type, payload }) {
  const handler = HANDLER_USER[type];
  if (handler) {
    yield handler(payload);
  }
}
function* sagaWatcherUsers() {
  yield takeEvery("*", sagaManage);
}

export default sagaWatcherUsers;
