import { BrowserRouter as Router, Route, Switch, Redirect } from "react-router-dom";
import React, { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import "./App.css";
import MainPage from "./views/components/pages/home-page/MainPage";
import SignUp from "./views/components/pages/registration-page/SignUp";
import Personal from "./views/components/pages/personal-page/index";
import SignIn from "./views/components/pages/authorization/SignIn";
import { setCurrentUser } from "./stores/action/usersAction";

const App = () => {
  const dispatch = useDispatch();
  const { currentUser, isAuth } = useSelector(state => state.users);
  useEffect(() => {
    dispatch(setCurrentUser(JSON.parse(localStorage.getItem("user"))));
  }, [dispatch]);
  return (
    <section className="app">
      <Router>
        <Switch>
          <Route exact path="/" component={MainPage} />
          <Route path="/login" component={SignUp}>
            {isAuth && <Redirect to={`/user/${currentUser.id}`} />}
          </Route>
          <Route path="/user/:id" component={Personal}>
            {!isAuth && <Redirect to="/" />}
          </Route>
          <Route path="/sign-in" component={SignIn}>
            {isAuth && <Redirect to={`/user/${currentUser.id}`} />}
          </Route>
        </Switch>
      </Router>
    </section>
  );
};
export default App;
