import React from "react";
import { useSelector, useDispatch } from "react-redux";
import Button from "@material-ui/core/Button";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import { Link, BrowserRouter } from "react-router-dom";

import { setCurrentUser } from "../../stores/action/usersAction";

export default function SimpleMenu() {
  const [anchorEl, setAnchorEl] = React.useState(null);
  const dispatch = useDispatch();
  const { isAuth } = useSelector(state => state.users);
  const handleClick = event => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const logout = () => {
    dispatch(setCurrentUser({}));
    localStorage.clear();
  };
  return (
    <div>
      <Button
        data-testid="open-menu"
        aria-controls="simple-menu"
        aria-haspopup="true"
        onClick={handleClick}
      >
        Open Menu
      </Button>
      <Menu
        id="simple-menu"
        data-testid="simple-menu"
        anchorEl={anchorEl}
        keepMounted
        open={Boolean(anchorEl)}
        onClose={handleClose}
      >
        {isAuth ? (
          <MenuItem data-testid="sign-out" href="/login" onClick={logout}>
            Sign out
          </MenuItem>
        ) : (
          <BrowserRouter>
            <Link to="/sign-in">
              <MenuItem data-testid="login" href="/login">
                Sign in
              </MenuItem>
            </Link>
            <Link to="/login">
              <MenuItem href="/login">Sign up</MenuItem>
            </Link>
          </BrowserRouter>
        )}
      </Menu>
    </div>
  );
}
