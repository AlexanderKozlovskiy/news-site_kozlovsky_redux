import React, { memo } from "react";
import { useSelector } from "react-redux";

import Card from "@material-ui/core/Card";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Typography from "@material-ui/core/Typography";

const Personal = () => {
  const {
    currentUser: { first_name, last_name },
  } = useSelector(state => state.users);

  return (
    <>
      <Card>
        <CardActionArea>
          <CardMedia />
          <CardContent>
            <Typography gutterBottom variant="h1" component="h2">
              {first_name}
              {last_name}
            </Typography>
          </CardContent>
        </CardActionArea>
        <CardActions />
      </Card>
    </>
  );
};

export default memo(Personal);
