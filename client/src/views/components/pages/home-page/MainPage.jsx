import React, { useEffect, useMemo, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Pagination } from "@material-ui/lab";
import { getNews } from "../../../../stores/action/newsAction";
import ListNews from "../../ListNews";
import SearchNews from "../../SearchNews";
import SimpleMenu from "../../SimpleMenu";

const MainPage = () => {
  const dispatch = useDispatch();
  const [allPages, setAllPages] = useState(0);
  const [currentPage, setCurrentPage] = useState(1);

  useEffect(() => {
    dispatch(getNews());
  }, [dispatch]);

  const { allNews, searchedValue } = useSelector(state => state.news);

  const normalizePages = filterArray => {
    const lastPage = Math.ceil(filterArray.length / 2);
    let newCurrentPage = currentPage > lastPage ? lastPage : currentPage;
    newCurrentPage = newCurrentPage < 1 ? 1 : newCurrentPage;
    setCurrentPage(newCurrentPage);
    setAllPages(lastPage);
  };

  const filterNews = useMemo(() => {
    const filterArray = allNews.filter(item =>
      Object.values(item).some(value =>
        String(value).toLowerCase().includes(searchedValue.toLowerCase())
      )
    );
    normalizePages(filterArray);

    return filterArray;
  }, [allNews, searchedValue]);

  const renderNews = useMemo(() => {
    const start = (currentPage - 1) * 2;
    const end = start + 2;
    return filterNews.slice(start, end);
  }, [currentPage, filterNews]);

  return (
    <>
      <div className="d-flex flex-column justify-content-center align-items-md-center">
        <div className="header">
          <h4> News </h4>
          <SimpleMenu />
        </div>
        <SearchNews />
        <ListNews news={renderNews} />
        <Pagination
          count={allPages}
          page={currentPage}
          shape="rounded"
          onChange={(event, clickedPages) => {
            setCurrentPage(clickedPages);
          }}
        />
      </div>
    </>
  );
};

export default MainPage;
