import React from "react";
import { useSelector } from "react-redux";
import Alert from "@material-ui/lab/Alert";
import { arrayOf, string, number, shape } from "prop-types";
import Card from "@material-ui/core/Card";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles({
  root: {
    maxWidth: 345,
  },
  media: {
    height: 140,
  },
});

const ListNews = ({ news }) => {
  const classes = useStyles();
  const { newsError } = useSelector(state => state.news);
  return (
    <>
      {newsError.length !== 0 ? (
        <Alert data-testid="error-alert" severity="error">
          {newsError.message}
        </Alert>
      ) : (
        <>
          {news.map(item => (
            <Card data-testid="news-card" key={item.id} className={classes.root}>
              <CardActionArea>
                <CardContent>
                  <Typography data-testid="card-title" gutterBottom variant="h4">
                    {item.title}
                  </Typography>
                  <br />
                  {item.description}
                </CardContent>
              </CardActionArea>
            </Card>
          ))}
        </>
      )}
    </>
  );
};

ListNews.propTypes = {
  news: arrayOf(
    shape({
      user: shape({
        e_mail: string,
        first_name: string,
        last_name: string,
      }),
      createdAt: string,
      description: string,
      id: number,
      image: string,
      tags: string,
      title: string,
      updatedAt: string,
      user_id: number,
    })
  ).isRequired,
};
export default ListNews;
