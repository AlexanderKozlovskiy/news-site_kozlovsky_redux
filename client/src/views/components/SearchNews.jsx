import { useSelector, useDispatch } from "react-redux";
import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import { handleSearchValue } from "../../stores/action/newsAction";

const useStyles = makeStyles(theme => ({
  root: {
    "& .MuiTextField-root": {
      margin: theme.spacing(1),
      width: "80vw",
    },
  },
}));

const SearchNews = () => {
  const { searchedValue } = useSelector(state => state.news);
  const dispatch = useDispatch();

  const classes = useStyles();
  const handleChange = event => {
    dispatch(handleSearchValue(event.target.value));
  };
  return (
    <form
      onSubmit={event => {
        event.preventDefault();
      }}
      className={classes.root}
      noValidate
      autoComplete="off"
    >
      <TextField
        id="filled-textarea"
        label="Search News"
        placeholder="Placeholder"
        value={searchedValue}
        onChange={handleChange}
      />
    </form>
  );
};
export default SearchNews;
